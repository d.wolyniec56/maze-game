var level= // level blocks
[
	[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
	[1,2,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,1,1,0,0,0,1,1,1,1,0,0,0,1,0,1,0,0,0,4,1],
	[1,1,1,0,1,0,1,1,1,0,1,1,1,0,0,0,1,0,1,0,1,0,0,1,0,0,0,0,1,0,1,0,0,0,1,0,1,1,0,1],
	[1,0,0,0,5,0,1,0,0,0,0,1,1,1,1,1,1,0,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,1],
	[1,0,1,1,1,1,1,1,1,1,0,0,0,0,0,0,1,0,1,0,1,0,0,0,0,0,1,0,1,0,0,0,1,0,1,0,0,1],
	[1,0,0,0,0,0,0,0,0,1,0,1,1,1,1,1,1,0,1,0,1,1,1,1,1,0,1,0,1,1,1,0,1,0,1,1,0,1],
	[1,1,1,1,1,1,1,1,0,1,0,0,0,0,0,0,1,0,1,0,1,0,0,0,1,0,1,0,1,0,0,0,1,0,0,0,0,1],
	[1,0,0,0,0,0,0,0,0,1,0,1,1,1,1,0,1,0,1,0,0,0,1,0,1,0,1,0,1,0,1,1,1,1,1,1,1,1],
	[1,0,1,1,1,1,1,1,1,1,0,1,0,0,0,0,1,0,0,0,1,1,1,0,0,0,0,0,0,0,0,0,1],
	[1,0,0,0,1,0,0,0,0,0,0,1,0,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,1,1,0,1],
	[1,1,1,0,1,1,0,1,1,3,1,1,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,1,1,0,1],
	[1,0,0,0,1,1,0,1,0,0,0,1,0,1,1,1,0,1,1,1,1,1,0,1,1,1,1,1,0,1,0,0,1],
	[1,1,0,1,1,0,0,1,1,1,1,1,1,1,1,1,0,1,0,0,0,1,0,1,1,0,1,1,0,1,0,1,1],
	[1,0,0,0,1,0,1,1,0,0,0,0,0,0,0,0,0,1,1,0,0,1,0,0,0,0,0,1,0,1,0,1,],
	[1,0,1,0,1,0,1,1,0,1,1,1,1,1,1,1,0,1,0,0,1,1,0,1,1,0,1,1,0,0,0,1,],
	[1,0,1,0,1,0,1,0,0,1,0,0,0,0,0,0,0,1,0,1,1,0,0,0,0,0,0,1,1,1,1,1,],
	[1,0,1,0,0,0,1,0,0,1,0,1,1,1,1,0,1,1,0,1,1,1,1,1,1,1,0,1,],
	[1,0,1,1,1,1,1,1,0,1,0,1,1,1,0,0,1,0,0,1,0,1,0,0,0,1,0,1,],
	[1,0,0,0,0,0,0,1,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,1,0,0,0,1,],
	[1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1],
]
var block=  //set block height and width
{
	h:25,
	w:25
}
var player= 
{
	col:0,
	row:0
}
var barricade=[1,3,4,5] // these are an ARRAY of block ids that act as barriers

var blockImages= // set up an OBJECT ({...} is an object ) for the images
{
	keyPart1:new Image(), // new Image() initializes the image
	keyPart2:new Image(),
	wall:new Image(),
	Player:new Image(),
	Lock:new Image ()
	
}												//		OBJECT		NODE   ATTRIBUTE			PATH TO IMAGE
blockImages.keyPart1.src = 'Images/KeyPart1.png'; // blockImages .keyPart1   .scr     =     'Images/KeyPart1.png'
blockImages.keyPart2.src = 'Images/KeyPart2.png';
blockImages.wall.src = 'Images/wall.png';
blockImages.Player.src = 'Images/Player.png';
blockImages.Lock.src = 'Images/Lock.png';




var canvas, context;
$(document).ready( function()
{
	setTimeout(function() 
	{
	
		canvas = document.getElementById('game');
		context = canvas.getContext('2d');

		paintFrame();
		document.onkeydown = checkKey;  //set up a listener when the key is pressed down(onkeydown) and when that happend run the function 'checkKey'
		function checkKey(e) // runs when a key is pressed
		{
			e = e || window.event; // get the event data and store it as 'e' the event is information about what just happended -  in this case we want to know what key was pressed
			if(e.keyCode == '38') //up
			{
				if (blockPassover(level[player.row-1][player.col])) //check if you can move there
				{
					level[player.row][player.col]=0; // if you can set current spot to empty - at this point you are no longer in existance
					player.row=player.row-1;	// then place yourself back in the game above where you were
				}
			}
			else if (e.keyCode == '40') //down
			{
				if (blockPassover(level[player.row+1][player.col]))		//		level[player.row+1][player.col]
				{
					level[player.row][player.col]=0;
					player.row=player.row+1;
				}
			}
			else if (e.keyCode == '37') //left
			{
				if (blockPassover(level[player.row][player.col-1]))
				{
					level[player.row][player.col]=0;
					player.col=player.col-1;
				}
			}
			else if (e.keyCode == '39') //right
			{
				if (blockPassover(level[player.row][player.col+1]))
				{
					level[player.row][player.col]=0;
					player.col=player.col+1
				}
			}	
			else if (e.keyCode == '32') //space
			{
				// here you can access a space press
			}
			//you can add any other key by looking up javaScript keyCodes
//line 105			
			level[player.row][player.col]=2 // set in the level data a 2 for where you are located to reflect your true spot for the paint
			paintFrame(); // paint with new spots
		}
	}, 500);	
});
paintFrame = function()
{
	
	context.clearRect(0,0,1000,500); // clear all images
	context.rect(0,0,1000,500);		// put background color in
	context.stroke();
	
	for (var i = 0; i<level.length;i++) //spin through the ROWS 1 at a time
	{
		for (var x = 0; x<level[i].length;x++) // inside the ROWS are COLUMNS.. this will spin thrugh the COLUMNS of the CURRENT ROW
		{
			var blockid=level[i][x]; // set the blockId = to the current ROW and COLUMN id (whatever number is in the big array at the top)
			paintBlock(blockid,i,x); // run a function that will paint the block based in the BlockId at the location of i, x which are the x, y coords... also the ROW, COLUMN
			if (blockid==2) // the set the spot of the player 
			{
				player.col=x;
				player.row=i;
			}
		}	
		
	}
	
	
}
paintBlock = function(type, row, col)
{
	if (type==0)
	{
		context.fillStyle="#C3CECF";
		context.fillRect( col*block.w, row*block.h, block.h, block.w )
	}
	else if (type==1)
	{
		context.drawImage(blockImages.wall, col*block.w, row*block.h);
	}
	else if (type==2)
	{
		context.drawImage(blockImages.Player, col*block.w, row*block.h);
	}
	else if (type==3)
	{
		context.drawImage(blockImages.Lock, col*block.w, row*block.h);
	}
	else if (type==4)
	{
		context.drawImage(blockImages.keyPart1, col*block.w, row*block.h);
	}
	else if (type==5)
	{
		context.drawImage(blockImages.keyPart2, col*block.w, row*block.h);
	}
	else
	{
		alert("Unknown block")
	}
}
blockPassover = function(type)
{
	for(var i=0; i <barricade.length;i++) // spin through the barricades and see if the block your about to move into will allow you to
	{
		if(barricade[i]==type)
			return false; // if you cant move there return false
	}
	return true; // if you spin through all of the blocks and you never hit an issue you can move there and return true
	
}


/*
	paint order
	
	can i move there?
	|-- yes- alter data to reflect you moving, then repaint screen
	|-- no - do nothing

	we want to add a new feature that will be an actions block 
	FIRST) remove 3,4,5 from the 'barricade' var
	
	then create a function and call it checkForActionBlocks that will take the type (0,1,2,3,4,5......)
	checkForActionBlocks = function(type)
	{
		
	}
	
	in there have 
	
	if(type == 3)
	{
		alert('you have picked up a key fragment')
	}
	you can add more blocks like this for the other 2 blocks, note that a 5 is a full key
	
	finally call the function when you paint the frame place it on line 105
	
	checkForActionBlocks(    place the type of the block your move onto     );
	
	you can get that by passing in --> level[player.row][player.col]

	after you do this you will be picking up keys although you will not be able to do anything with them this is the first step



*/











